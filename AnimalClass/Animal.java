public class Animal {
    public String name;             // Name uses String since it probably have more than one character
    public AnimalType animalType;   // Animal type as an enum to make it readable yet useable in code logic
    public short age;               // A giant galapagos tortoise can apprently live up to around 175 years, so byte (max 127) won't cut it
    public float weight;            // Weight is most likely using decimals. The precision needed probably won't warrant the usage of double
    public Gender gender;           // A boolean could of worked here, but enum covers better and is more descriptive
    public byte legAmount;          // It's unlikely that an animal would have more than 127 legs (At least that's how I motivate it for this assignment)
    public boolean fur;             // The animal either has fur or does not, perfect fit for a boolean
    public String noise;            // For this implementation, noise can be null if it doesn't make a noise, otherwise it contains a string of the noise it makes
}

public enum AnimalType {
    Cat,
    Dog,
    Goose,
    Tortoise
}

public enum Gender {
    Male,
    Female,
    Other
}