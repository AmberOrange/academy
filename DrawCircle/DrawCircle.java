import java.util.Scanner;

public class DrawCircle {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in).useDelimiter(" ");
        System.out.print(">");
        int diameter = sc.nextInt();
        double radius = diameter / 2.0;
        //boolean[][] canvas = new boolean[radius][radius];

        for(int y = 0; y < diameter; y++) {
            for(int x = 0; x < diameter; x++) {
                double distance = Math.hypot((double)x - radius + 0.5, (double)y - radius + 0.5);
                //System.out.print(distance+"\t");
                if(distance > radius - 3.0 && distance < radius) {
                    //canvas[x][y] = true;
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }
    }
}