import java.util.ArrayList;

public class CharacterCreator {
    public static void main(String[] args) {
        // Make a new character party array list
        ArrayList<Character> party = new ArrayList<Character>();
        // Add four classes with random stats
        party.add(new Warrior("Roll Fizzlebeef"));
        party.add(new Ranger("Cupid"));
        party.add(new Wizard("Gandalf"));
        party.add(new Warrior("Leroy Jenkins"));

        // Pick a random character
        Character pickedCharacter = party.get((int)(Math.random() * 4));

        // Display the character's stats
        pickedCharacter.displayStats();
        System.out.println("");

        // Have them attack
        pickedCharacter.attack();
    }
}