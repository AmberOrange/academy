public class Ranger extends Character {
    public Ranger(String name) {
        super(name);    // Fills in the name through the superclass constructor
        className = "Ranger";

        // Randomize a max point amount between 15-20
        int attributePoints = (int)(Math.random() * 5 + 15);

        // The ranger's primary stat is speed: (Half max points + 0 to quater max points variance)
        speed = (int)(Math.random() * attributePoints / 4.0 + attributePoints / 2.0);
        attributePoints -= speed;   // Remove points from the max
        // Next stat: (A third max points + 0 to a third max points variance)
        intelligence = (int)(Math.random() * attributePoints / 3.0 + attributePoints / 3.0);
        // Last stat gets the remainder
        toughness = attributePoints - intelligence;
    }

    // Class-specific attack message
    public void attack() {
        System.out.printf("%s draws their bow!", name);
    }

    public String printClassName() {
        return className;
    }
}