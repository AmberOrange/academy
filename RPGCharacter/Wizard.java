public class Wizard extends Character {
    public Wizard(String name) {
        super(name);    // Fills in the name through the superclass constructor
        className = "Wizard";

        // Randomize a max point amount between 15-20
        int attributePoints = (int)(Math.random() * 5 + 15);

        // The wizard's primary stat is intelligence: (Half max points + 0 to quater max points variance)
        intelligence = (int)(Math.random() * attributePoints / 4.0 + attributePoints / 2.0);
        attributePoints -= intelligence;    // Remove points from the max
        // Next stat: (A third max points + 0 to a third max points variance)
        toughness = (int)(Math.random() * attributePoints / 3.0 + attributePoints / 3.0);
        // Last stat gets the remainder
        speed = attributePoints - toughness;
    }

    // Class-specific attack message
    public void attack() {
        System.out.printf("%s casts the only spell they know: fireball!", name);
    }

    public String printClassName() {
        return className;
    }

    
}