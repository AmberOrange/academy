public class Warrior extends Character {
    public Warrior(String name) {
        super(name);    // Fills in the name through the superclass constructor
        className = "Warrior";

        // Randomize a max point amount between 15-20
        int attributePoints = (int)(Math.random() * 5 + 15);

        // The warrior's primary stat is toughness: (Half max points + 0 to quater max points variance)
        toughness = (int)(Math.random() * attributePoints / 4.0 + attributePoints / 2.0);
        attributePoints -= toughness;   // Remove points from the max
        // Next stat: (A third max points + 0 to a third max points variance)
        speed = (int)(Math.random() * attributePoints / 3.0 + attributePoints / 3.0);
        // Last stat gets the remainder
        intelligence = attributePoints - speed;
    }

    // Class-specific attack message
    public void attack() {
        System.out.printf("%s swings their mighty axe!", name);
    }

    public String printClassName() {
        return className;
    }
}