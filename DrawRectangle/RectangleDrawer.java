import java.util.Scanner;

public class RectangleDrawer {
    public static void main(String[] args) {
        // Create new scanner, reading from the in stream
        Scanner sc = new Scanner(System.in);
        // Declare and init default variables
        int width = 5, height = 5, x = 0, y = 0;
        char symbol = '#';
        
        // Get input from user
        width = getIntInput("Enter Width: ", sc);

        height = getIntInput("Enter Height: ", sc);

        symbol = getCharInput("Enter Symbol: ", sc);

        // Check to see if there's any point to print anything
        if(height > 0 && width > 0) {
            // Write the top wall of the rectangle
            for(x = 0; x < width; x++) {
                System.out.print(symbol);
            }
            // Prepare new row
            System.out.print('\n');

            // A loop for each "left-right" wall
            for(y = 1; y < height - 1; y++) {
                // If there's at least one character to write...
                if(width > 1) {
                    // Write one symbol
                    System.out.print(symbol);
                    // Add spaces
                    for(x = 1; x < width -1; x++) {
                        System.out.print(' ');
                    }
                    // If the width is more than two, we know a right wall should be added
                    if(width > 2) {
                        System.out.print(symbol);
                    }
                }
                // Prepare new row
                System.out.print("\n");
            }
            
            // Write the bottom wall if the height allows it
            for(x = 0; x < width && y < height; x++) {
                System.out.print(symbol);
            }

        }
    }
    public static int getIntInput(String message, Scanner sc) {
        // To be returned
        int returnValue = 0;
        // False as long as the input isn't valid
        boolean accepted = false;

        // Loop until we've achieved a valid input
        while(!accepted) {
            // User input has potential to not be valid, try...
            try {
                System.out.print(message);
                returnValue = sc.nextInt();
                accepted = true;
            } catch(Exception e)
            {
                // If there's an input mismatch, catch exception...
                System.out.println("Not a valid input, please try again.");
                // ... and empty out the scanner since it still contains the previous output
                sc.nextLine();
            }
        }

        return returnValue;
    }

    public static char getCharInput(String message, Scanner sc) {
        // To be returned
        char returnValue = '#';
        // False as long as the input isn't valid
        boolean accepted = false;

        // Loop until we've achieved a valid input
        while(!accepted) {
            // User input has potential to not be valid, try...
            try {
                System.out.print(message);
                returnValue = sc.next().charAt(0);
                accepted = true;
            } catch(Exception e)
            {
                // If there's an input mismatch, catch exception...
                System.out.println("Not a valid input, please try again.");
                // ... and empty out the scanner since it still contains the previous output
                sc.nextLine();
            }
        }

        return returnValue;
    }
}