![Experis Academy Banner](cover-academy.jpg)
## Experis Academy Assignments

This collection of projects are assignments originally submitted through the Moddle platform individually.
Since the usage of *git* was not mandatory at that time, I've decided to upload them for safekeeping.