public class Person {
    public String fullName;         // Full name as string
    public short birthYear;         // Short is suitable for numbers ranging around the thousands 
    public byte birthMonth;         // Byte works well for month and day since the numbers are small
    public byte birthDay;           //      Sidenote: What you could do is to make a separate class for birthday containing these three
    public String telephoneNumber;  // A phone number can have symbols like +/- that is not possible to represent with an integer

    // One can argue that a "default constructor" is not necessary as you would probably want to
    // add a proper entry to the people list, but the potential is still there to edit this object
    // manually before adding it to the array
    public Person() {
        this.fullName = "Nobody";
        this.birthYear = 0;
        this.birthMonth = 0;
        this.birthDay = 0;
        this.telephoneNumber = "";
    }

    // Takes in all the details and writes it to the member variables
    public Person(String fullName, int birthYear, int birthMonth, int birthDay, String telephoneNumber) {
        this.fullName = fullName;
        this.birthYear = (short) birthYear;
        this.birthMonth = (byte) birthMonth;
        this.birthDay = (byte) birthDay;
        this.telephoneNumber = telephoneNumber;
    }
}