import java.util.ArrayList;

public class PeopleList {
    // This list won't be used by anything except this class's member functions, thus private
    // But it could be desirable having it as public if this was a bigger application
    private static ArrayList<Person> personList;

    public static void main(String[] args) {
        // Instantiate the array list
        personList = new ArrayList<Person>();

        // Add all made up persons
        personList.add(new Person("Jonas Murkwood", 1994, 5, 21, "074-12343231"));
        personList.add(new Person("Walter Gray", 1921, 2, 26, "+4276934234"));
        personList.add(new Person("Sheva Nagami", 1926, 7, 11, "054 423 421 21"));
        personList.add(new Person("Adam Francis", 1963, 3, 23, "+44 42 534 321"));
        personList.add(new Person("Sarah Iceberg", 1981, 12, 7, "052304234"));

        // To make sure that all persons were added correctly, iterate and print the information
        for(Person person : personList) {
            System.out.printf("%s\tBirthday: %d-%d-%d\tPhone number: %s\n",
                person.fullName,
                person.birthYear,
                person.birthMonth,
                person.birthDay,
                person.telephoneNumber);
        }
    }
}