import pets.*;

public class Program {

    public static void main(String[] args) {
        Pet fluffy = new Pet("Fluffy", "Craig", 4, PetType.cat);
        System.out.println("Name: " + fluffy.name);
        System.out.println("Owner: " + fluffy.owner);
        System.out.println("Pet Type: " + fluffy.petType);
        String hello = String.format("Hello! %s", "Hello");
    }
}