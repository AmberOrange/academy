package pets;

public class Pet {
    public String name;
    public String owner;
    public int age;
    public PetType petType;

    // Creates a pet with a specific name, and unknown owner or age
    public Pet(String name){
        this.name = name;
        owner = "Unknown";
    }

    // Creates a pet with a specific name, owner, age, and pet type
    public Pet(String name, String owner, int age, PetType petType){
        this.name = name;
        this.owner = owner;
        this.age = age;
        this.petType = petType;
    }
}