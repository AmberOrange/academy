package pets;

public enum PetType {
    cat,
    dog,
    fish,
    rat
}