

public class Greeting {
    public static void main(String[] args) {
        IntegerTest();
        ListTesting test = new ListTesting();
        test.runTest();
    }

    private static void IntegerTest() {
        System.out.println("Hello world!");
        int test = Integer.MAX_VALUE;
        System.out.println("test: " + test);
        test++;
        System.out.println("test: " + test);
        Integer test2 = Integer.MAX_VALUE;
        System.out.println("test2: " + test2);
        test2++;
        System.out.println("test2: " + test2);
    }
}