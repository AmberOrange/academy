import java.util.ArrayList;

public class ListTesting {
    public ListTesting() {
        // This is a constructor!
        System.out.println("This is the \"ListTesting\" constructor!");
    }

    public void runTest() {
        ArrayList<String> names = new ArrayList<String>();

        names.add("Emma");
        names.add("Jakob");
        names.add("Lucas");
        names.add("Sophie");

        System.out.println(names.get(1));

        for(String name : names) {
            System.out.println(name);
        }
    }
}