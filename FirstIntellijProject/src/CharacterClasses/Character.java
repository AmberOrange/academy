package CharacterClasses;

public abstract class Character {
    // All member variables are protected so that they can be accessed from subclasses
    protected String name;
    protected int toughness;
    protected int speed;
    protected int intelligence;
    protected String className;

    // Takes the name from the subclass and fills it in
    public Character(String name) {
        this.name = name;
    }

    // Prints out the stats
    // Can optionally print out the stat total
    public void displayStats() {
        System.out.printf("%s the %s\n\n", name, className);
        System.out.printf("Tough: %d\n", toughness);
        System.out.printf("Speed: %d\n", speed);
        System.out.printf("Intel: %d\n", intelligence);
        //System.out.printf("Total: %d\n", toughness+speed+intelligence);
    }

    public abstract void attack();
    public abstract String printClassName();
}