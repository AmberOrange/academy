public class RectangleModulo {
    public static void main(String[] args) {
        int width = 31, height = 31;

        for(int y = 0; y < width; y++) {
            for(int x = 0; x < height; x++) {
                if(x%2==0 && Math.min(x,height-x-1) <= y && y <= Math.max(x,height-x-1)) {
                    System.out.print("#");
                } else
                if (Math.min(y, width-y-1) <= x && x <= Math.max(y, width-y-1) && y%2==0) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }
    }
}