package foodDiet;

import animals.Animal;

// Common for all diets:
// All diets implements a unique function with a unique name that returns
// what kind of food they eat

public abstract class Omnivore extends Animal{
    public String eatAnything() {
        return "I'm not picky, everything's tasty";
    }
}