package foodDiet;

import animals.Animal;

// Common for all diets:
// All diets implements a unique function with a unique name that returns
// what kind of food they eat

public abstract class Herbivore extends Animal {
    public String eatPlants() {
        return "These crops sure are tasty";
    }
}