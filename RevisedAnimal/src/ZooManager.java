import locomotion.*;

import java.util.ArrayList;
import java.util.Random;

import animals.*;
import foodDiet.*;

public class ZooManager {
    public static void main(String[] args) {
        // Init a random class
        Random rand = new Random();
        // Make a new animal list
        ArrayList<Animal> animalList = new ArrayList<Animal>();

        // Add all available animals
        animalList.add(new Cat());
        animalList.add(new Mouse());
        animalList.add(new Salmon());
        animalList.add(new Sheep());
        animalList.add(new Eagle());
        
        // Pick one random animal
        Animal pickedAnimal = animalList.get(rand.nextInt(animalList.size()));

        // Make the animal do a noise
        pickedAnimal.makeNoise();

        // Print the animal's diet
        System.out.println("This animal's diet:");
        printDiet(pickedAnimal);

        // Print the mode of locomotion
        System.out.println("This animal's modes of locomotion:");
        printLocomotives(pickedAnimal);
    }

    // This function checks for the diet type and call the appropriate
    // eat function
    private static void printDiet(Animal animal) {
        if (animal instanceof Carnivore) {
            System.out.println(((Carnivore) animal).eatMeat());
        } else if (animal instanceof Herbivore) {
            System.out.println(((Herbivore) animal).eatPlants());
        } else if (animal instanceof Omnivore) {
            System.out.println(((Omnivore) animal).eatAnything());
        } else {
            // This should not be called if everything is working right
            System.out.println("This animal doesn't eat apparently...");
        }
    }

    // This function checks for every type of locomotion interface
    // and call its function
    private static void printLocomotives(Animal animal) {
        if (animal instanceof Walkable) {
            System.out.println(((Walkable) animal).walk());
        }
        if (animal instanceof Runable) {
            System.out.println(((Runable) animal).run());
        }
        if (animal instanceof Swimable) {
            System.out.println(((Swimable) animal).swim());
        }
        if (animal instanceof Flyable) {
            System.out.println(((Flyable) animal).fly());
        }
    }
}