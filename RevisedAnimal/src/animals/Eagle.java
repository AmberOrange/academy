package animals;

import foodDiet.Carnivore;
import locomotion.Flyable;

// Common for all animal subclasses:
// The animal implements their own version of the Animal class "makeNoise()"
// and the interface methods (like walk and run)

public class Eagle extends Carnivore implements Flyable {

    public String fly() {
        return "The eagle soars through the skies!";
    }

    public void makeNoise() {
        System.out.println("Screech");
    }
    
}