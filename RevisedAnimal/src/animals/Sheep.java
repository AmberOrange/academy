package animals;

import foodDiet.*;
import locomotion.*;

// Common for all animal subclasses:
// The animal implements their own version of the Animal class "makeNoise()"
// and the interface methods (like walk and run)

public class Sheep extends Herbivore implements Walkable, Runable {
    public Sheep() {
    }

    public void makeNoise() {
        System.out.println("Baa");
    }

    public String run() {
        return "The sheep runs from the wolf!";
    }

    public String walk() {
        return "The sheep walks slowly to the next patch of grass.";
    }
}