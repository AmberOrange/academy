package animals;

public abstract class Animal {
    // makeNoise will be implemented by the individual subclass animals
    public abstract void makeNoise();
}