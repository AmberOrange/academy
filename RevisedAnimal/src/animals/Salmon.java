package animals;

import foodDiet.Carnivore;
import locomotion.Swimable;

// Common for all animal subclasses:
// The animal implements their own version of the Animal class "makeNoise()"
// and the interface methods (like walk and run)

public class Salmon extends Carnivore implements Swimable {

    public String swim() {
        return "The salmon swims upstreams gracefully!";
    }

    public void makeNoise() {
        System.out.println("Blub");
    }
    
}