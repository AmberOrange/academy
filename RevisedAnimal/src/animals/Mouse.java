package animals;

import foodDiet.*;
import locomotion.*;

// Common for all animal subclasses:
// The animal implements their own version of the Animal class "makeNoise()"
// and the interface methods (like walk and run)

public class Mouse extends Omnivore implements Walkable, Runable {
    public void makeNoise() {
        System.out.println("Squeek");
    }

    public String run() {
        return "The mouse attempts to run from the predators!";
    }

    public String walk() {
        return "The mouse silently walks up to the food source.";
    }
}