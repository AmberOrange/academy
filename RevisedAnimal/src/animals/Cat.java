package animals;

import foodDiet.*;
import locomotion.*;

// Common for all animal subclasses:
// The animal implements their own version of the Animal class "makeNoise()"
// and the interface methods (like walk and run)

public class Cat extends Carnivore implements Walkable, Runable {
    public void makeNoise() {
        System.out.println("Meow");
    }

    public String run() {
        return "The cat run to hunt its prey.";
    }

    public String walk() {
        return "The cat walks around a bowl of porridge.";
    }
}