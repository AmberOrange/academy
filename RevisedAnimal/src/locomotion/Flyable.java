package locomotion;

// Common for all locomotions:
// Defines a unique function describing a mode of transportation

public interface Flyable {
    public String fly();
}