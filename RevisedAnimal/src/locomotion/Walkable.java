package locomotion;

// Common for all locomotions:
// Defines a unique function describing a mode of transportation

public interface Walkable {
    public String walk();
}