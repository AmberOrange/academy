import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TextFileSearcher {
    public static void main(String[] args) {
        File targetFile;
        String fileName;
        
        System.out.println("Please input a file name (preferably a .txt):");
        System.out.print(">");
        
        // The scanner class doesn't throw any exceptions if used with
        // "System.in" it appears, so no need to check for them here
        Scanner sc = new Scanner(System.in);
        // Read the next line from the user input
        fileName = sc.nextLine();

        // Try opening the file.
        // If it doesn't exist or any other errors occurs,
        // there's not need to continue running the program
        try {
            targetFile = new File(fileName);
                if(!targetFile.exists() || !targetFile.isFile()) {
                    throw new FileNotFoundException("The specified file does not exist");
                }
        } catch(Exception ex) {
            System.out.printf("An exception was throw when attempting to open target file:\n%s\n", ex.getMessage());
            sc.close();
            return;
        }
        
        // Print file path and file size
        System.out.printf("%s absolute file path:\n%s\n", fileName, targetFile.getAbsolutePath());
        System.out.printf("File size (kb): %d\n", targetFile.length() / 1024);

        // Check if the file got the ".txt" extension
        if(getFileExtension(fileName).compareTo("txt") == 0)
        {
            String wordToSearch;
            int wordCounter = 0;
            
            // Get input from user
            System.out.print("Enter a word to search for:\n>");
            wordToSearch = sc.next();
            
            // Try to read the file, catch if the file couldn't be found
            try (Scanner fileScanner = new Scanner(targetFile)){
                // Do file operation
                try {
                    String word;

                    // Loop through every word, compare to the search word and increment the counter
                    while(fileScanner.hasNext()) {
                        word = fileScanner.next();
                        if(word.contains(wordToSearch)) {
                            wordCounter++;
                        };
                    }

                    // Print results
                    System.out.printf("%s was mentioned %d times", wordToSearch, wordCounter);
                }
                catch (Exception ex) {
                    // Handle other exceptions
                    System.out.printf("Exception thrown when attempting to read the file:\n%s\n", ex.getMessage());
                }
                finally {
                    try {
                            // Close the file
                            fileScanner.close();
                        }
                        catch (Exception ex) {
                            // Handle exceptions during close
                            System.out.printf("Exception thrown when attempting to close file:\n%s\n", ex.getMessage());
                        }
                }
    
            }
            catch (FileNotFoundException ex) {
                // Handle other exceptions
                System.out.printf("A file not found exception was thrown:\n%s\n", ex.getMessage());
            }

        }

        // Finally, close the "System.in" scanner
        sc.close();
    }

    // This function takes the file name and returns the text after '.'
    public static String getFileExtension(String fileName) {
        int index = fileName.lastIndexOf('.');
        return (index == -1) ? "" : fileName.substring(index + 1);
    }
}