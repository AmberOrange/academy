package se.experis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Program {

    // A class to contain the code and name pair in the same location
    private static class CodePair {
        public String code;
        public String name;
        public CodePair(String code, String name) {
            this.code = code;
            this.name = name;
        }
        public String toString() {
            if(name != null) {
                return String.format("%s,%s", this.code, this.name);
            } else {
                return this.code;
            }
        }
    }
    public static void main(String[] args) {

        File file;

        // Attempt to find the file "UnusedCodes.txt" from the resource folder, get the path and open it
        try {
            file = new File(Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("UnusedCodes.txt")).getPath());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return;
        }

        // Early exit if the file doesn't exist
        if(!file.exists()) {
            System.out.println("UnusedCodes.txt can't be found in the resource folder");
            return;
        }

        // Array of code pairs to be loaded in
        ArrayList<CodePair> codePairs = new ArrayList<>();

        // Try to scan a file
        // Should not throw, but it's required to be handled
        try {
            Scanner scanner = new Scanner(file);

            // Go line by line
            while(scanner.hasNextLine()) {
                CodePair entry;
                String code;
                String name;
                // Scan the line and separate using ','
                Scanner lineScanner = new Scanner(scanner.nextLine()).useDelimiter(",");
                code = lineScanner.hasNext() ? lineScanner.next() : "";     // Check if there's a code to read
                name = lineScanner.hasNext() ? lineScanner.next() : null;   // Check if there's a name to read
                entry = new CodePair(code, name);
                codePairs.add(entry);   // Add the pair to the list
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // For debugging purposes: Prints all read pairs
//        for (CodePair pair :
//                codePairs) {
//            System.out.println(pair.toString());
//        }

        // Get input from user
        Scanner inputScanner = new Scanner(System.in);
        String userName;

        System.out.print("Please provide your name:\n>");
        userName = inputScanner.nextLine();

        System.out.println("Your name is " + userName);

        CodePair match = null;
        CodePair emptyPair = null;
        for (CodePair pair :
                codePairs) {
            if (pair.name == null) {        // The first pair that doesn't have a name
                if (emptyPair == null) {    // will be a candidate if we need to add a new user
                    emptyPair = pair;
                }
                // If we found a match, store the reference and stop looking
            } else if (pair.name.contains(userName)) {
                match = pair;
                break;
            }
        }

        // Print the key if a match was found
        if(match != null) {
            System.out.printf("%s has already been assigned a key:\n%s", match.name, match.code);
        } else if(emptyPair != null) {  // Otherwise, update the file and display the assigned key
            emptyPair.name = userName;
            try {
                FileWriter fileWriter = new FileWriter(file);
                for (CodePair pair :
                        codePairs) {
                    fileWriter.write(pair.toString()+"\n");
                }
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.printf("%s has been registered! Assigned key:\n%s", emptyPair.name, emptyPair.code);
        } else {    // Otherwise, there's no more keys and Craig needs to be contacted
            System.out.println("No keys remains. Please contact Craig");
        }
    }
}
