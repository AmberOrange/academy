import java.io.FileReader;
import java.util.EmptyStackException;
import java.util.Stack;

/**
 * Teaming up with Emil Grenebrant & Jacob Wickström
 */
public class CheckBalanced {

    public static void main(String[] args) {
        // Try, because file reader can throw
        try {
            // Open this source file to check
            FileReader fileReader = new FileReader("CheckBalanced.java");
            int data;
            char character;
            // When the special mode is engaged, wait for closing symbol specific to the
            // special index
            boolean special = false;
            byte specialIndex = -1;
            // This implementation uses a stack to keep track of latest added open symbol
            Stack<Character> stack = new Stack<Character>();

            try {
                // Read character by character until end of file
                while ((data = fileReader.read()) != -1) {
                    character = (char) data;
                    // If escape symbol, skip the next following char
                    if (character == '\\') {
                        fileReader.read();
                    } else {
                        // If the special mode is not engaged, check if it should be
                        if (special == false) {
                            if (character == '\"') {
                                special = true;
                                specialIndex = 0;
                            } else if (character == '\'') {
                                special = true;
                                specialIndex = 1;
                            } else if (character == '/') {
                                character = (char) fileReader.read();
                                if (character == '/') {
                                    special = true;
                                    specialIndex = 2;
                                } else if (character == '*') {
                                    special = true;
                                    specialIndex = 3;
                                }
                            }
                        } else {    // If the special mode is engaged, check if it shouldn't be
                            switch (specialIndex) {
                                case 0:
                                    if (character == '\"') {
                                        special = false;
                                    }
                                    break;
                                case 1:
                                    if (character == '\'') {
                                        special = false;
                                    }
                                    break;
                                case 2:
                                    if (character == '\n') {
                                        special = false;
                                    }
                                    break;
                                case 3:
                                    if (character == '*') {
                                        character = (char) fileReader.read();
                                        if (character == '/') {
                                            special = false;
                                        }
                                    }
                                    break;
                            }
                        }

                        // Don't look for symbols if the special mode is engaged
                        if (special == false) {
                            normalSearch(stack, character);
                        }
                    }

                }

                // If the stack is empty (no unclosed symbols) and the special mode is not on (no comments, strings etc.)...
                if (stack.empty() && special == false) {
                    // ... This file passes!
                    System.out.println("The file is balanced, all OK!");
                } else {    // Otherwise, throw an exception
                    throw new Exception("We finished with open symbols, UNBALANCED");

                }
            } catch (Exception e) {
                // Print exception messages
                System.out.printf("Exception when reading file thrown:\n%s\n", e.getMessage());
            }
        } catch (Exception e) {
            // Print exception messages
            System.out.printf("Exception when loading file thrown:\n%s\n", e.getMessage());

        }
    }

    public static void normalSearch(Stack<Character> stack, char character) throws Exception {
        // The stack may be empty, encapsulate with try
        try {
            switch (character) {
                case '{':   // If any of these open symbols are present
                case '(':   // add to stack
                case '[':
                case '<': // for part 2

                    stack.add(character);
                    break;
                case '}':   // If a closing symbol don't have a corresponding opening symbol at the top of the stack, throw error
                    if (stack.pop() != '{') {
                        throw new Exception("No balanced match found, reason: " + character);
                    }
                    break;
                case ')':
                    if (stack.pop() != '(') {
                        throw new Exception("No balanced match found, reason: " + character);
                    }
                    break;
                case ']':
                    if (stack.pop() != '[') {
                        throw new Exception("No balanced match found, reason: " + character);
                    }
                    break;
                case '>':
                    if (stack.pop() != '<') {
                        throw new Exception("No balanced match found, reason: " + character);
                    }
                    break;

            }
        } catch (EmptyStackException ese) {
            throw new Exception("Too many closing symbols!");   // Custom message for empty stack exception
        }
    }
}
