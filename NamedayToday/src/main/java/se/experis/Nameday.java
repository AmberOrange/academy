package se.experis;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

public class Nameday {
    public static void main(String[] args) {
        String countryCode = "se";
        String timezoneCode = "Europe/Amsterdam";

        try {
            
            URL url = new URL("https://api.abalin.net/today?country="+countryCode+"&timezone="+timezoneCode);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            if(con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuilder content = new StringBuilder();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();

                JSONObject json = new JSONObject(content.toString());
                System.out.println(json.toString(4));
                System.out.println("Today's nameday in Sweden:");
                System.out.println(json.getJSONObject("data").getJSONObject("namedays").getString("se"));
                //json.getJSONObject("nameDays").getString("se");
            } else {
                System.out.println("Error");
                System.out.println("Server responded with: " + con.getResponseCode());
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}